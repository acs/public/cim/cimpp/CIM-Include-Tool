#include <iostream>
#include <chrono>
#include <unordered_map>
#include <vector>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <sstream>
#include <tclap/CmdLine.h>
#include <regex>

/** @file */
namespace fs = boost::filesystem;

/** This function adds where possible a relative path to all include statements
 *
 * @param str is the filebuffer which is modified
 * @param map contains all paths
 * @param filename is the name of the file which is modified, only used for debugging messenges
 */
void replace_includes(std::string& str, std::unordered_map<std::string, fs::path>& map, const std::string& filename);

/** Function to replace enum declarations with enum class declarations
 *
 * @param str is the filebuffer which is modified
 * @param filename is the name of the file which is modified, only used for debugging messenges
 */
unsigned int replace_enum(std::string& str, const std::string& filename);

/** Function to add "#include <list>" to a file containing std::list
 *
 * @param str is the filebuffer which is modified
 */
void add_std_inc(std::string& str);

/** Function to remove UUID include guard and replace them with standard include guards
 *
 * @param str is the filebuffer which is modified
 * @param filename is the name of the file which is modified, only used for debugging messenges
 */
void remove_uuid(std::string& str, const std::string& filename);

/** Function to remove "// Created on: ... " line comments
 *
 * @param str is the filebuffer which is modified
 * @param filename is the name of the file which is modified, only used for debugging messenges
 */
void remove_created_on(std::string& str, const std::string& filename);

std::ostringstream log_stream;

int main(int argc, char *argv[])
{
	std::string inputPath;
	bool doEnums;
	try
	{
		// For information about tclap see: http://tclap.sourceforge.net/
		TCLAP::CmdLine cmd("...", ' ', "1.0");
		TCLAP::SwitchArg enumSwitch("", "enable-enum-fix", "Enables fixing enum to enum class in headers", cmd);
		TCLAP::UnlabeledValueArg<std::string> inputPathArg("input-path", "The path in which the folder IEC61970 is located in", true, "", "path", cmd);
		cmd.parse(argc, argv);
		inputPath = inputPathArg.getValue();
		doEnums = enumSwitch.getValue();
	}
	catch (TCLAP::ArgException &e)  // catch any exceptions
	{ std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }

	std::chrono::time_point<std::chrono::high_resolution_clock> start, stop;
	start = std::chrono::high_resolution_clock::now();


	fs::path p(inputPath);

	std::unordered_map<std::string, fs::path> headers_map; // map to store paths to the headers with the name as key
	std::vector<fs::path> headers_vec; // vector that stores all header files
	std::vector<fs::path> source_vec; // vector that stores all source files

	// Find all headers in path p
	try
	{
		if(fs::is_directory(p))
		{
			if(!fs::is_directory(fs::path(std::string(inputPath).append("/IEC61970"))))
			{
				std::cout << inputPath << " does not contain folder IEC61970" << std::endl;
				std::cout << "Use --help for more information" << std::endl;
				return 3;
			}
			for(fs::recursive_directory_iterator it(p); it != fs::recursive_directory_iterator(); it++)
			{
				if(fs::is_regular_file(*it) && it->path().extension() == ".h")
				{
					// Return false if there is already a file with the same name in the map
					if(headers_map.emplace(it->path().filename().string(), fs::relative(it->path(), p)).second == false)
					{
						log_stream << it->path().filename() << std::endl;
						log_stream << "Header müssen einen eindeutigen Namen besitzen" << std::endl;
						return 1;
					}
					headers_vec.push_back(it->path());
				}
				if(fs::is_regular_file(*it) && it->path().extension() == ".cpp")
				{
					source_vec.push_back(it->path());
				}
			}
		}
		else
		{
			std::cout << inputPath << "is not a path" << std::endl;
			std::cout << "Use --help for more information" << std::endl;
			return 2;
		}
	}
	catch(const fs::filesystem_error& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	int enums = 0; // numer of replaced enums
	for(int i = 0; i < headers_vec.size(); i++)
	{
		std::fstream file;
		file.open(headers_vec.at(i).string(), std::fstream::in);
		std::string str;
		str.reserve(fs::file_size(headers_vec.at(i)));
		str.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
		file.close();

		replace_includes(str, headers_map, headers_vec.at(i).string());
		if(doEnums)
			enums += replace_enum(str, headers_vec.at(i).string());
		add_std_inc(str);

		remove_uuid(str, headers_vec.at(i).string());

		remove_created_on(str, headers_vec.at(i).string());

		file.open(headers_vec.at(i).string(), std::fstream::out | std::fstream::trunc);
		file << str;
		file.close();

		// Processbar
		std::cout << "\r";
		std::cout << "["<< std::string(60*(i+1)/(headers_vec.size() + source_vec.size()), '*') << std::string(60-60*(i+1)/(headers_vec.size() + source_vec.size()), ' ') << "]";
		std::cout << i+1;
		std::cout.flush();
	}

	for(int i = 0; i < source_vec.size(); i++)
	{
		std::fstream file;
		file.open(source_vec.at(i).string(), std::fstream::in);
		std::string str;
		str.reserve(fs::file_size(source_vec.at(i)));
		str.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
		file.close();

		remove_created_on(str, source_vec.at(i).string());

		file.open(source_vec.at(i).string(), std::fstream::out | std::fstream::trunc);
		file << str;
		file.close();

		// Processbar
		std::cout << "\r";
		std::cout << "["<< std::string(60*(i+1+headers_vec.size())/(headers_vec.size() + source_vec.size()), '*') << std::string(60-60*(i+1+headers_vec.size())/(headers_vec.size() + source_vec.size()), ' ') << "]";
		std::cout << i+1+headers_vec.size();
		std::cout.flush();
	}

	std::cout << "\n";
	std::cout << log_stream.str();
	if(doEnums)
		std::cout << enums << " enums gefunden" << std::endl;
	stop = std::chrono::high_resolution_clock::now();
	auto dur = stop - start;
	std::cout << headers_vec.size() + source_vec.size() << " Dateien gefunden und bearbeited in ";
	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(dur).count() << "ms" << std::endl;
	return 0;
}

unsigned int replace_enum(std::string& str, const std::string& filename)
{
	unsigned int count = 0;
	std::string::size_type pos_begin, pos_end;
	pos_begin = 0;
	while(true)
	{
		pos_begin = str.find_first_of("e/", pos_begin);
		if(pos_begin == std::string::npos)
			break;
		if(str.at(pos_begin) == '/')
		{
			if(str.at(pos_begin + 1) == '/')
			{
				pos_begin = str.find('\n', pos_begin);
				if(pos_begin == std::string::npos)
					break;
				continue;
			}
			if(str.at(pos_begin + 1) == '*') // /* gefunden
			{
				pos_begin = str.find("*/", pos_begin);
				if(pos_begin == std::string::npos)
					break;
				continue;
			}
		}
		if(str.substr(pos_begin, 4) == "enum")
		{
			if(str.substr(pos_begin, 10) == "enum class")
			{
				pos_end = str.find_first_of("\n", pos_begin);
				//log_stream << "In Datei " << filename << ": " << str.substr(pos_begin, pos_end-pos_begin) << std::endl;
			}
			else
			{
				pos_end = str.find_first_of("\n", pos_begin);
				//log_stream << "In Datei " << filename << ": " << str.substr(pos_begin, pos_end-pos_begin);
				str.replace(pos_begin, 4, "enum class");
				pos_end = str.find_first_of("\n", pos_begin);
				//log_stream << " mit " << str.substr(pos_begin, pos_end-pos_begin) << " ersetzt" << std::endl;
			}

			pos_begin++;
			count++;
		}
		else
			pos_begin++;
	}
	return count;
}

void replace_includes(std::string& str, std::unordered_map<std::string, fs::path>& map, const std::string& filename)
{
	std::string::size_type pos_begin, pos_end;
	pos_begin = 0;
	while(true)
	{
		pos_begin = str.find("#include", pos_begin);
		if(pos_begin == std::string::npos)
			break;
		if(str.substr(pos_begin, 15) == "#include <list>")
		{
			pos_begin++;
			continue;
		}
		pos_begin = str.find_first_of("<\"", pos_begin) + 1;
		pos_end = str.find_first_of(">\\/\"", pos_begin);
		if(str.at(pos_end) == '/' || str.at(pos_end) == '\\')
		{
			pos_end = str.find_first_of(">\"", pos_begin);
			//log_stream << str.substr(pos_begin, pos_end-pos_begin) << " ist bereits ein Pfad" << std::endl;
			continue;
		}

		std::unordered_map<std::string, fs::path>::const_iterator it = map.find(str.substr(pos_begin, pos_end-pos_begin));
		if(it == map.end())
		{
			log_stream << "In Datei " << filename << ": Konnte " << str.substr(pos_begin, pos_end-pos_begin) << " nicht finden" << std::endl;
		}
		else
		{
			str = str.replace(pos_begin, pos_end-pos_begin, it->second.string());
		}
	}
}

void add_std_inc(std::string& str)
{
	if(str.find("std::list") != std::string::npos)
	{
		if(str.find("#include <list>") != std::string::npos)
		{
			return;
		}
		else
		{
			std::string::size_type pos;
			if(str.find("#include") != std::string::npos)
			{
				pos = str.find("#include");
			}
			else
			{
				pos = str.find("namespace");
			}
			str.insert(pos, "#include <list>\n\n");
			return;
		}
	}
}

void remove_uuid(std::string& str, const std::string& filename)
{
	std::string modified_filename(filename);
	modified_filename = modified_filename.substr(modified_filename.find_last_of("/") + 1);
	boost::to_upper(modified_filename);
	boost::replace_all(modified_filename, ".", "_");

	//std::cout << modified_filename << std::endl;
	std::regex expression1("#if !defined\\(EA.*__INCLUDED_\\)");
	std::string replacement1("#ifndef ");
	replacement1.append(modified_filename);
	std::regex expression2("#define EA.*__INCLUDED_");
	std::string replacement2("#define ");
	replacement2.append(modified_filename);
	std::regex expression3("#endif // !defined\\(EA.*__INCLUDED_\\)");
	std::string replacement3("#endif // ");
	replacement3.append(modified_filename);
	str = std::regex_replace(str, expression1, replacement1);
	str = std::regex_replace(str, expression2, replacement2);
	str = std::regex_replace(str, expression3, replacement3);
}

void remove_created_on(std::string& str, const std::string& filename)
{
	std::regex expression1("//  Created on:.*\\n");
	str = std::regex_replace(str, expression1, "");
}
