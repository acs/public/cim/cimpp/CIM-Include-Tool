# CIM-Include-Tool

The CIM-Include-Tool adds include directives for `std::list`. In addition
include directives without relative path will be completed, e.g. `#include "Terminal.h"`
becomes `#include "IEC61970/Base/Core/Terminal.h"`. This assures that when
calling the complier with `-I <GeneratedCodePath>` the compiler will find all
CIM C++ files.

#### Dependencies
* [Boost](http://www.boost.org/) >= 1.60.0
* [tclap](http://tclap.sourceforge.net/) >= 1.2

#### Build instructions:
* Create and change into build directory  

```bash
mkdir build
cd build
```
* Call cmake to generate Makefile

```bash
cmake ..
```
* Call make to build project

```bash
make
```


#### Usage:
```
CIM-Include-Tool  [--enable-enum-fix] [--version] [-h] <path>
```
The option `--enable-enum-fix` tries to change all occurrences of `enum`
definitions to `enum class` definitions. This option should not be used. Instead
use CIMRefactorer for this.
